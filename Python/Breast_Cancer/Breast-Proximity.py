import csv
import math
from random import shuffle
    

#the inverse of euclidean distance      
def getEuclideanProximity(sample, sample_test):
    distance = 0
    for x in range(len(sample)-1):#last value is excluded in distance computation
        distance += pow((float(sample[x]) - float(sample_test[x])), 2)
    res=math.sqrt(distance)
    return 1/res if res!= 0 else 0 

#the inverse of absolute distance      
def getAbsoluteProximity(sample, sample_test):
    distance = 0
    for x in range(len(sample)-1):#last value is excluded in distance computation
        distance += abs(float(sample[x]) - float(sample_test[x]))
    return 1/distance if distance!= 0 else 0 

#test Accuracy of prediction against TestSet
def getAccuracy(predictions):
    correct = 0
    for x in range(len(TestSet)-1):
        #[-1] means that tuple will be indexed from the end
        if str(TestSet[x][-1]) == str(predictions[x]):
            correct += 1
    return str((correct/float(len(TestSet)))*100.0)

#returns the predicted weighted value
def getWeightedVal(query):
    proximity=benign=malignant=0

    for sample in TS:
        proximity = getAbsoluteProximity(sample,query)
        if str(sample[-1])=='2': benign+=proximity
        else: malignant+=proximity
        
    return '2' if benign>malignant else '4'
 
def runNW():
    predictions=[]
    #consider FOR EACH QUERY all samples by weighting their value with proximity
    for query in TestSet:
       predictions.append(getWeightedVal(query))
    #return predictions
    return getAccuracy(predictions)

#runs cycles of tests
def k_fold_test(cycles,perc):

    global file
    global TS
    global TestSet
    global Split
    val=0
    Split=perc

    for x in range(cycles):
        
        TS=[]
        TestSet=[]
    
        #sorting by random
        shuffle(file)

        #taking TS lines 80% of total
        ts_lines = int((len(file)-1)*Split)
        
        for line in range(ts_lines):
            TS.append(file[line])
            
        #populating Test Set
        for line in range(len(file)-ts_lines):
            TestSet.append(file[line])
            
        TestSet.sort()
        current=float(runNW())
        #print(str(current)+"%")
        val+=float(current)
            
    print("Average: "+str(val/cycles)+"%")

       
        
with open('../../Breast-dataset/breast-cancer-wisconsin.data', 'r') as csv_file:
    
    file=[]
    TS=[]
    TestSet=[]
    Split=0.80 # percentage of TS
    i=0
    
    csv_reader = csv.reader(csv_file, delimiter=',')
    
    #dataset includes some '?' values for unknown values
    #we remove those rows in order to prevent exception in data manipulation
    for line in csv_reader:
        #excluding first column which contains IDs
        #factor each line into tuple for immutability
        #data manipulation on tuple is much faster than on lists
        if not '?' in line:
            file.insert(i,tuple(line[1:])) 
            i+=1
                
    #sorting by random
    shuffle(file)

    #taking TS lines 80% of total
    ts_lines = int((len(file)-1)*Split)
    
    for line in range(ts_lines):
        TS.append(file[line])
        
    #populating Test Set
    for line in range(len(file)-ts_lines):
        TestSet.append(file[line])
        
    TestSet.sort()
    
    #range of feature values based on first feature
    RANGE = set(range(len(TS[0])))
    